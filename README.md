# godog-service

**Description**<br/>
Service to get and post godog scenario information

**Install Package**<br/>
`$ make package`

**Running**<br/>
`$ make run`

**Contact**<br/>
`mpermperpisang@gmail.com`
