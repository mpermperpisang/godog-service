package data

type godogData struct {
	feature  string
	platform string
	pwd      string
}

var godog godogData

// SetFeature : set data for feature tags list
func SetFeature(feature string) {
	godog.feature = feature
}

// GetFeature : get data for feature tags list
func GetFeature() string {
	return godog.feature
}

// SetPlatform : set data for platform name detail
func SetPlatform(platform string) {
	godog.platform = platform
}

// GetPlatform : get data for platform name detail
func GetPlatform() string {
	return godog.platform
}

// SetPWD : set data for directory path
func SetPWD(path string) {
	godog.pwd = path
}

// GetPWD : get data for directory path
func GetPWD() string {
	return godog.pwd
}
