all: kill-port run

package:
	@go get -u github.com/gorilla/mux
	@echo "Package installed"

kill-port:
	@kill -9 $$(lsof -t -i:8383)
	@echo "Port 8383 is killed"

run:
	@echo "Running code"
	go run main.go
