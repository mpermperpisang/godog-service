package postservice

import (
	"net/http"
	"net/url"

	"gitlab.com/godog-service/helper/data"
)

// SetGodogInfo : set cucumber run information
func SetGodogInfo(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()

	FeatureTags(params)
	PlatformName(params)
	PWDPath(params)
}

// FeatureTags : feature tags list
func FeatureTags(params url.Values) error {
	data.SetFeature(params.Get("executionTags"))

	return nil
}

// PlatformName : platform name detail
func PlatformName(params url.Values) error {
	data.SetPlatform(params.Get("platformName"))

	return nil
}

// PWDPath : directory path
func PWDPath(params url.Values) error {
	data.SetPWD(params.Get("pwdPath"))

	return nil
}
