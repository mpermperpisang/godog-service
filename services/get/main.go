package getservice

import (
	"net/http"
	"strings"

	"gitlab.com/godog-service/helper/data"
)

// GetGodogInfo : get cucumber run information
func GetGodogInfo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(`{
		"feature_tags": "` + data.GetFeature() + `",
		"platform_name": "` + platformName() + `",
		"directory": "` + data.GetPWD() + `"
	}`))
}

func platformName() string {
	name := strings.Trim(data.GetPlatform(), " ")

	return name
}
