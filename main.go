package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	getservice "gitlab.com/godog-service/services/get"
	postservice "gitlab.com/godog-service/services/post"
)

func main() {
	r := mux.NewRouter()

	r.HandleFunc("/godog-support", postservice.SetGodogInfo).Methods(http.MethodPost)
	r.HandleFunc("/godog-support", getservice.GetGodogInfo).Methods(http.MethodGet)
	log.Fatal(http.ListenAndServe(":8383", r))
}
